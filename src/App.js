import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Clean Gitlab's Package Registry from CI</h1>
        <p>
          This repo is intended to show you how to automatically clean old packages stored in Gitlab's Package Registry, using Gitlab CI and Gitlab's API.
        </p>
        <p>
          Take a look at the following files:
        </p>
        <ul>
          <li><code>.gitlab-ci.yml</code></li>
          <li><code>gitlab-ci/delete_old_gitlab_packages.sh</code></li>
        </ul>
      </header>
    </div>
  );
}

export default App;
